package vra7tenant

import (
	"errors"
	"fmt"
	"log"
)

var ErrTenantNotFound = errors.New("vra7tenant:Tenant Not Found")

//TenantTemplate - This struct holds blueprint response of Tenant
type TenantInfo struct {
	TenantID      string `json:"id"`
	URLName       string `json:"urlName"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	ContactEmail  string `json:"contactEmail"`
	Password      string `json:"password"`
	DefaultTenant bool   `json:"defaultTenant"`
}

//TenantID- This struct holds Tenant name from json response.
type TenantID struct {
	Name string `json:"id"`
}

//TenantUrlName - This struct holds Tenant name from json response.
type TenantUrlName struct {
	Name string `json:"name"`
}

//TenantName - This struct holds the value of response of Tenant item list
type TenantName struct {
	TenantName string `json:"TenantItem"`
}

//TenantDescription - This struct holds the value of response of Tenant item list
type TenantDescription struct {
	TenantDescription string `json:"TenantDescription"`
}

//TenantContactEmail - This struct holds the value of response of Tenant item list
type TenantContactEmail struct {
	TenantContactEmail string `json:"TenantContactEmail"`
}

//TenantPassword - This struct holds the value of response of Tenant item list
type TenantPassword struct {
	TenantPassword string `json:"TenantPassword"`
}

//TenantDefaultTenant - This struct holds the value of response of Tenant item list
type TenantDefaultTenant struct {
	TenantDefaultTenant bool `json:"TenantDefaultTenant"`
}

//GetTenant - set call to read Tenant item provided in terraform config file
func (c *APIClient) GetTenant(tenantID string) (*TenantInfo, error) {
	//Form a path to read Tenant template via REST call

	path := fmt.Sprintf("/identity/api/tenants/%s", tenantID)
	log.Printf("GetTenant->path %v\n", path)

	tenantInfo := new(TenantInfo)
	apiError := new(APIError)
	//Set REST call to get Tenant template
	getTenantRes, err := c.HTTPClient.New().Get(path).Receive(tenantInfo, apiError)

	if getTenantRes.StatusCode == 404 {
		//Means we tried to get a tenant that doesn't exist
		log.Printf("GetTenant-> returning ErrTenantNotFound for tenantID [%s]", tenantID)
		return nil, ErrTenantNotFound
	}
	if err != nil {
		log.Printf("GetTenant-> err !=nil:%v", err)
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("GetTenant-> apiErr !isEmpty: %s\n", apiError.Error())
		return nil, apiError
	}

	//Return Tenant template
	log.Printf("GetTenant->returning found Tenant  %v\n", tenantInfo)
	return tenantInfo, nil
}

//GetTenant - set call to read Tenant item provided in terraform config file
//This compliles but hasnb't been actually user/tested yet (may be useful for import?
func (c *APIClient) GetAllTenants() ([]TenantInfo, error) {
	//Form a path to read Tenant template via REST call

	path := fmt.Sprintf("/identity/api/tenants")
	log.Printf("GetAllTenant->path %v\n", path)

	var tenantInfo []TenantInfo
	apiError := new(APIError)
	//Set REST call to get Tenant template
	_, err := c.HTTPClient.New().Get(path).Receive(tenantInfo, apiError)

	/*	if getTenantRes.StatusCode == 404 {
		//Means we tried to get a tenant that doesn't exist
		log.Printf("GetTenant-> returning ErrTenantNotFound for tenantID [%s]", tenantID)
		return nil, ErrTenantNotFound
	}*/
	if err != nil {
		log.Printf("GetTenant-> err !=nil:%v", err)
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("GetTenant-> apiErr !isEmpty: %s\n", apiError.Error())
		return nil, apiError
	}

	//Return Tenant template
	log.Printf("GetTenant->returning found Tenant  %v\n", tenantInfo)
	return tenantInfo, nil
}
