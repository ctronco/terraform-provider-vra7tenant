package vra7tenant

import (
	"errors"
	"fmt"
	"log"
	//"reflect"

	"encoding/json"

	"github.com/hashicorp/terraform/helper/schema"
)

//ResourceVra7Tenant - use to set resource fields
func ResourceVra7Tenant() *schema.Resource {
	return &schema.Resource{
		Create: createResourceVra7Tenant,
		Read:   readResourceVra7Tenant,
		Update: updateResourceVra7Tenant,
		Delete: deleteResourceVra7Tenant,
		Schema: setResourceVra7TenantSchema(),
	}
}

//set_resource_schema - This function is used to update the tenant
//and replace the values with user defined values added in .tf file.
func setResourceVra7TenantSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"tenant_id": {
			Type:        schema.TypeString,
			Required:    true,
			Description: "Tenant ID usually matches url_name ",
		},
		"url_name": {
			Type:        schema.TypeString,
			Required:    true,
			Description: "the  url name component(ex:vra.example.com/vcac/org/<url_name>",
		},
		"name": {
			Type:        schema.TypeString,
			Required:    true,
			Description: "The name of the tenant",
		},
		"description": {
			Type:        schema.TypeString,
			Optional:    true,
			Description: "Description of the tenant",
		},
		"contact_email": {
			Type:        schema.TypeString,
			Optional:    true,
			Description: "Contact Email for the tenant",
		},
		"password": {
			Type:        schema.TypeString,
			Optional:    true,
			Description: "Password (unused)",
		},
		"default_tenant": {
			Type:        schema.TypeBool,
			Optional:    true,
			Default:     false,
			Description: "Is this the default tenant (ex: vsphere.local)",
		},
	}
}

//Function use - to set a create resource call
//Terraform call - terraform apply
func createResourceVra7Tenant(d *schema.ResourceData, meta interface{}) error {
	//func createResourceVra7Tenant(d *schema.ResourceData) error {
	//Log file handler to generate logs for debugging purpose
	//Get client handle
	client := meta.(*APIClient)

	// if we're not logged into the system_tenant we can't create other tenants so bail
	if !client.SystemTenant {
		return fmt.Errorf("Resource Tenant Request Failed: Provider not System Tenant")
	}
	//If catalog_name and catalog_id both not provided then throw an error
	if len(d.Get("url_name").(string)) <= 0 && len(d.Get("description").(string)) <= 0 && len(d.Get("contact_email").(string)) <= 0 {
		return fmt.Errorf("Missing One or more required values from: [id, url_name, description, contact_email]")
	}
	log.Printf("createResourceVra7Tenant-> Checking to see if Tenant [%s] already exists", d.Get("tenant_id").(string))
	//Set a  create Tenant function call
	//checkTenant, err := client.GetTenant(d.Get("tenant_id").(string))
	_, err := client.GetTenant(d.Get("url_name").(string))
	if err != ErrTenantNotFound {
		// Let's Check and make sure there wasn't another error
		if err != nil {
			log.Printf("createResourceVra7Tenant -> Error getting Tenant: %v", err)
			log.Printf("createResourceVra7Tenant->TenantId %v\n", d.Get("tenant_id").(string))
			//Set a  create Tenant function call
			//var tenantInfo TenantInfo
		} else {
			log.Printf("Tenant may alreay exist: Resource Tenant Request Failed: %v", err)
			log.Printf("createResourceVra7Tenant->TenantId %v\n", d.Get("tenant_id").(string))
			//Set a  create Tenant function call
			//var tenantInfo TenantInfo
		}

	} else {
		log.Printf("createResourceVra7Tenant-> CreatingTenant: %v", err)
		tenantInfo := new(TenantInfo)
		//At least as of 7.3  tenant_id and url_name are two separate fields, but need to match
		// when creating a tenant otherwise you get: "90026-Tenant id and urlName do not match.",
		tenantInfo.TenantID = d.Get("url_name").(string)
		tenantInfo.URLName = d.Get("url_name").(string)
		tenantInfo.Name = d.Get("name").(string)
		tenantInfo.Description = d.Get("description").(string)
		tenantInfo.ContactEmail = d.Get("contact_email").(string)
		tenantInfo.Password = d.Get("password").(string)
		tenantInfo.DefaultTenant = d.Get("default_tenant").(bool)
		requestTenant, err := client.CreateVra7Tenant(tenantInfo)
		if err == nil {
			log.Printf("print before block %s", requestTenant.TenantID)
		} else {
			return fmt.Errorf("Resource Tenant Request Failed: %v", err)
		}

	}

	//Set request ID
	//d.SetId(requestTenant2.TenantID)
	d.SetId(d.Get("url_name").(string))
	//Set request status
	return nil
}

//Function use - to update centOS 6.3 Tenant present in state file
//Terraform call - terraform refresh
func updateResourceVra7Tenant(d *schema.ResourceData, meta interface{}) error {
	log.Printf("updateResourceVra7Tenant->Updating: %v", d)
	log.Println(d)
	readTenantID := d.Id()
	//Get client handle
	client := meta.(*APIClient)
	//Get requested status
	oldTenantInfo, err := client.GetTenant(readTenantID)
	//Raise an exception if error occured while fetching request status
	if err != nil {
		return fmt.Errorf("Resource view failed to load:  %v", err)
	}
	log.Printf("updateResourceVra7Tenant->Old TenantInfo: %v", oldTenantInfo)

	log.Printf("updateResourceVra7Tenant-> UpdatingTenant")
	newTenantInfo := new(TenantInfo)
	newTenantInfo.TenantID = oldTenantInfo.TenantID //d.Get("tenant_id").(string)
	newTenantInfo.URLName = oldTenantInfo.URLName   // d.Get("url_name").(string)
	newTenantInfo.Name = d.Get("name").(string)
	newTenantInfo.Description = d.Get("description").(string)
	newTenantInfo.ContactEmail = d.Get("contact_email").(string)
	newTenantInfo.Password = d.Get("password").(string)
	newTenantInfo.DefaultTenant = oldTenantInfo.DefaultTenant //d.Get("default_tenant").(bool)
	log.Printf("updateResourceVra7Tenant->New TenantInfo: %v", newTenantInfo)
	_, err = client.CreateVra7Tenant(newTenantInfo)
	if err != nil {
		return err
	}

	return nil

}

//readResourceVra7Tenant -- Reads tenant
//Function use - To read configuration of Tenant present in state file
//Terraform call - terraform refresh
func readResourceVra7Tenant(d *schema.ResourceData, meta interface{}) error {
	//Get requester Tenant ID from schema.dataresource
	readTenantID := d.Id()
	//Get client handle
	client := meta.(*APIClient)
	//Get requested status
	tenantInfo, err := client.GetTenant(readTenantID)
	//Raise an exception if error occured while fetching existing tenant info
	if err != nil {
		return fmt.Errorf("Resource view failed to load:  %v", err)
	}
	log.Printf("readResourceVra7Tenant->TenantInfo: %v", tenantInfo)
	//tenantid and url name shouldn't be changeable
	d.Set("name", tenantInfo.Name)
	d.Set("description", tenantInfo.Description)
	d.Set("contact_email", tenantInfo.ContactEmail)
	d.Set("password", tenantInfo.Password)
	d.Set("default_tenant", tenantInfo.DefaultTenant)
	return nil
}

//Function use - To delete resources which are created by terraform and present in state file
//Terraform call - terraform destroy
func deleteResourceVra7Tenant(d *schema.ResourceData, meta interface{}) error {
	//Get requester Tenant ID from schema.dataresource
	tenantID := d.Id()
	//Get client handle
	client := meta.(*APIClient)
	log.Printf("deleting tenant %s", tenantID)
	tenantInfo, err := client.GetTenant(tenantID)
	if err != nil {
		log.Printf("deleteResourceVra7Tenant-> Tenant [%s] Not Found", tenantID)
		return fmt.Errorf("Resource not found")
	}

	log.Printf("trying to delete tenant %s", tenantInfo.TenantID)
	err = client.DestroyVra7Tenant(tenantInfo)
	//Error
	if err != nil {
		return err
	}

	// No error, tenant was deleted successfully
	log.Printf("deleteResourceVra7Tenant->Tenant %s deleted", tenantInfo.TenantID)
	d.SetId("")
	return nil
}

//RequestVra7Tenant - To set create resource REST call
func (c *APIClient) CreateVra7Tenant(tenantInfo *TenantInfo) (*TenantInfo, error) {
	//Form a path to set a REST call to create a Tenant
	path := fmt.Sprintf("/identity/api/tenants/%s", tenantInfo.TenantID)

	createTenantRes := new(TenantInfo)
	apiError := new(APIError)

	jsonBody, jErr := json.Marshal(tenantInfo)
	if jErr != nil {
		log.Printf("Error marshalling template as JSON")
		return nil, jErr
	}
	log.Printf("CreateVra7Tenant->JSON Request Info: %s", jsonBody)
	log.Printf("CreateVra7Tenant->BearerToken = %s", c.BearerToken)

	//Set a REST call to create a Tenant
	createRes, err := c.HTTPClient.New().Put(path).BodyJSON(tenantInfo).
		Receive(createTenantRes, apiError)

		//	_, err := c.HTTPClient.New().Post(path).BodyJSON(tenantInfo).
		//		Receive(createTenantRes, apiError)
	log.Printf("createResponse was: %v", createRes)
	if err != nil {
		log.Printf("Error POSTing")
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("Error  apiErr POSTing")
		return nil, apiError
	}

	return createTenantRes, nil
}

//RequestVra7Tenant - To set create resource REST call
func (c *APIClient) DestroyVra7Tenant(tenantInfo *TenantInfo) error {
	//Form a path to set a REST call to create a Tenant
	path := fmt.Sprintf("/identity/api/tenants/%s", tenantInfo.TenantID)

	apiError := new(APIError)
	apiSuccess := new(TenantInfo)
	//Set a REST call to create a Tenant
	//createRes, err := c.HTTPClient.New().Delete(path).BodyJSON(tenantInfo).
	destroyRes, err := c.HTTPClient.New().Delete(path).Receive(apiSuccess, apiError)

	//Expect a status code of 204 if the delete was successful.
	if destroyRes.StatusCode == 204 {
		return nil
	}

	log.Printf("destroyResponse was: %v", destroyRes)
	if err != nil {
		log.Printf("Error DELETEing: %v", err)
		return err
	}

	return errors.New("Something weird happened")
}
