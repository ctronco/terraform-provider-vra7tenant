package vra7tenant

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"github.com/hashicorp/terraform/helper/schema"
)

//ResourceVra7TenantUser - use to set resource fields
func ResourceVra7TenantUser() *schema.Resource {
	return &schema.Resource{
		Create: createResourceVra7TenantUser,
		Read:   readResourceVra7TenantUser,
		Update: updateResourceVra7TenantUser,
		Delete: deleteResourceVra7TenantUser,
		Schema: setResourceVra7TenantUserSchema(),
	}
}

//set_resource_schema - This function is used to update the catalog template/blueprint
//and replace the values with user defined values added in .tf file.
func setResourceVra7TenantUserSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"first_name": {
			Type:     schema.TypeString,
			Required: true,
		},
		"last_name": {
			Type:     schema.TypeString,
			Required: true,
		},
		"email_address": {
			Type:     schema.TypeString,
			Required: true,
		},
		"description": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"locked": {
			Type:     schema.TypeBool,
			Optional: true,
		},
		"disabled": {
			Type:     schema.TypeBool,
			Optional: true,
		},
		"password": {
			Type:     schema.TypeString,
			Required: true,
		},
		"principal_id": {
			Type:        schema.TypeMap,
			Required:    true,
			Description: "Form of   <name> @ <domain>",
			Elem: &schema.Resource{
				Schema: map[string]*schema.Schema{
					"name": {
						Type:     schema.TypeString,
						Required: true,
					},
					"domain": {
						Type:     schema.TypeString,
						Required: true,
					},
				},
			},
		},
		"tenant_name": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"name": {
			Type:     schema.TypeString,
			Optional: true,
		},
	}
}

//Function use - to set a create resource call
//Terraform call - terraform apply
func createResourceVra7TenantUser(d *schema.ResourceData, meta interface{}) error {
	//Log file handler to generate logs for debugging purpose
	log.Println("createResourceVra7TenantUser->Starting")
	client := meta.(*APIClient)
	userInfo := new(UserInfo)
	userInfo.FirstName = d.Get("first_name").(string)
	userInfo.LastName = d.Get("last_name").(string)
	userInfo.Description = d.Get("description").(string)
	userInfo.EmailAddress = d.Get("email_address").(string)
	userInfo.Locked = d.Get("locked").(bool)
	userInfo.Disabled = d.Get("disabled").(bool)
	userInfo.Password = d.Get("password").(string)
	principalInfo := d.Get("principal_id").(map[string]interface{})
	userInfo.PrincipalID.Domain = principalInfo["domain"].(string)
	userInfo.PrincipalID.Name = principalInfo["name"].(string)
	userInfo.TenantName = d.Get("tenant_name").(string)
	//for principalFields := range principalInfo {
	//	log.Printf("PRincipalID[%s]=%s", principalFields, principalInfo[principalFields])
	//		userInfo.PrincipalID.[principalFields] = principalInfo[principalFields]
	//}

	//userInfo.TenantName = d.Get("tenant_name").(string)
	userInfo.Name = d.Get("name").(string)
	requestUser, err := client.CreateVra7TenantUser(userInfo)

	if err != nil {
		log.Printf("createResourceVra7TenantUser-> err !=nil:%v", err)
		return err
	}
	log.Printf("createResourceVra7TenantUser %s", requestUser.Name)

	//Set request ID
	//tmpID :=
	d.SetId(userInfo.PrincipalID.Name + "@" + userInfo.PrincipalID.Domain)
	return nil

}

//Function use - to update centOS 6.3 machine present in state file
//Terraform call - terraform refresh
func updateResourceVra7TenantUser(d *schema.ResourceData, meta interface{}) error {
	log.Println(d)
	return nil
}

//Function use - To read configuration of centOS 6.3 machine present in state file
//Terraform call - terraform refresh
func readResourceVra7TenantUser(d *schema.ResourceData, meta interface{}) error {
	//Get requester machine ID from schema.dataresource
	userID := d.Id()
	//Get client handle
	client := meta.(*APIClient)
	//Get requested status
	userInfo, err := client.GetUser(userID)

	//Raise an exception if error occured while fetching request status
	if err != nil {
		return fmt.Errorf("Resource view failed to load:  %v", err)
	}
	d.Set("name", userInfo.Name)

	return nil
}

//Function use - To delete resources which are created by terraform and present in state file
//Terraform call - terraform destroy
func deleteResourceVra7TenantUser(d *schema.ResourceData, meta interface{}) error {
	//Get requester machine ID from schema.dataresource
	//	userID := d.Id()
	//Get client handle
	//s	client := meta.(*APIClient)

	//Through an error if request ID has no value or empty value
	if len(d.Id()) == 0 {
		return fmt.Errorf("Resource not found")
	}

	d.SetId("")
	return nil
}

//CreateVra7TenantUser - To set create resource REST call
func (c *APIClient) CreateVra7TenantUser(userInfo *UserInfo) (*UserInfo, error) {
	//Form a path to set a REST call to create a Tenant
	log.Println("CreateVra7TenantUser->Starting")
	//Figure out wheter we've got a specified tenant_name (for case where system user is creating a local user in a subtenant)
	//or if we're just putting it into the tenant associated with the provider.
	path := ""
	if userInfo.TenantName == "" {
		path = fmt.Sprintf("/identity/api/tenants/%s/principals", c.Tenant)
	} else {
		path = fmt.Sprintf("/identity/api/tenants/%s/principals", userInfo.TenantName)
	}
	log.Printf("CreateVra7TenantUser->Path = [%s]", path)
	createUserRes := new(UserInfo)
	apiError := new(APIError)

	jsonBody, jErr := json.Marshal(userInfo)
	if jErr != nil {
		log.Printf("CreateVra7TenantUser->Error marshalling template as JSON")
		return nil, jErr
	}
	log.Printf("CreateVra7TenantUser->JSON Request Info: %s", jsonBody)
	log.Printf("CreateVra7TenantUser->BearerToken = %s", c.BearerToken)

	//Set a REST call to create a Tenant
	createRes, err := c.HTTPClient.New().Post(path).BodyJSON(userInfo).
		Receive(createUserRes, apiError)

		//	_, err := c.HTTPClient.New().Post(path).BodyJSON(tenantInfo).
		//		Receive(createTenantRes, apiError)
	log.Printf("CreateVra7TenantUser->createResponse was: %v", createRes)
	if createRes.StatusCode == 405 {
		return nil, errors.New("Create User Operation Not Allowed in SSO2 Mode")
	}
	if err != nil {
		log.Printf("CreateVra7TenantUser->Error POSTing")
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("CreateVra7TenantUser->Error  apiErr POSTing")
		return nil, apiError
	}

	return createUserRes, nil
}
