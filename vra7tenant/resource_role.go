package vra7tenant

import (
	"fmt"
	"log"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/nu7hatch/gouuid"
)

const (

	//Scope Roles
	ScopeRoleBusinessGroupManager    = "CSP_SUBTENANT_MANAGER"
	ScopeRoleBasicUser               = "CSP_CONSUMER"
	ScopeRoleSupportUser             = "CSP_SUPPORT"
	ScopeRoleEnterpriseAdministrator = "COM_VMWARE_IAAS_SCOPE_ENTERPRISE_ADMINISTRATOR"
	ScopeRoleRepositoryAdministrator = "COM_VMWARE_IAAS_SCOPE_REPOSITORY_ADMINISTRATOR"
	ScopeRoleDesignCenterUser        = "COM_VMWARE_IAAS_SCOPE_DESIGN_CENTER_USER"
	ScopeRoleRepositoryUser          = "COM_VMWARE_IAAS_SCOPE_REPOSITORY_USER"
	ScopeRoleDEMUser                 = "COM_VMWARE_IAAS_SCOPE_DEM_USER"
	ScopeRoleUserWithUIAccess        = "COM_VMWARE_IAAS_SCOPE_USER_WITH_UI_ACCESS"
	ScopeRoleWebsiteBootstrapper     = "COM_VMWARE_IAAS_SCOPE_WEBSITE_BOOTSTRAPPER"
	ScopeRoleServices                = "COM_VMWARE_IAAS_SCOPE_SERVICE"
	ScopeRoleAccessControlAdmin      = "COM_VMWARE_IAAS_SCOPE_ACCESSCONTROLADMIN"
	ScopeRoleFullAccessAdministrator = "COM_VMWARE_IAAS_SCOPE_FULL_ACCESS_ADMINISTRATOR"
	ScopeRoleDEMAccess               = "COM_VMWARE_IAAS_SCOPE_DEM_ACCESS"
	//Real "name" is {com.vmware.csp.core.cafe.identity@csp.scoperole.sharedaccess.user.name}
	ScopeRoleCSPConsumerWithSharedAccess = "CSP_CONSUMER_WITH_SHARED_ACCESS"

	//Tenant Roles
	RoleSecureExportConsumer    = "CONTENT_SERVICE_SECURE_EXPORT_CONSUMER"
	RoleSoftwareArchitect       = "SOFTWARE_SERVICE_SOFTWARE_ARCHITECT"
	RoleXaaSArchitect           = "DESIGNER_SERVICE_ARCHITECT"
	RoleCatalogAdministrator    = "CATALOG_SERVICE_CATALOG_ADMIN"
	RoleContainerAdmin          = "CONTAINER_ADMIN"
	RoleContainerArchitect      = "CONTAINER_ARCHITECT"
	RoleApplicationArchitect    = "COMPOSITION_SERVICE_APPLICATION_ARCHITECT"
	RoleInfrastructureArchitect = "COMPOSITION_SERVICE_INFRASTRUCTURE_ARCHITECT"
	RoleHealthConsumer          = "HEALTHBROKER_ADMIN"

	//System Roles
	//
	RoleRepositoryAdministrator = "COM_VMWARE_IAAS_REPOSITORY_ADMINISTRATOR"
	RoleDesignCenterUser        = "COM_VMWARE_IAAS_DESIGN_CENTER_USER"
	RoleSystemAdministrator     = "CSP_SYSTEM_ADMIN"
	RoleTenantAdministrator     = "CSP_TENANT_ADMIN"
	RoleApprovalAdministrator   = "CSP_APPROVAL_ADMIN"
	RoleRepositoryUser          = "COM_VMWARE_IAAS_REPOSITORY_USER"
	RoleDEMUser                 = "COM_VMWARE_IAAS_DEM_USER"
	RoleUserWithUIAccess        = "COM_VMWARE_IAAS_USER_WITH_UI_ACCESS"
	RoleWebsiteBootstrapper     = "COM_VMWARE_IAAS_WEBSITE_BOOTSTRAPPER"
	RoleEnterpriseAdministrator = "COM_VMWARE_IAAS_ENTERPRISE_ADMINISTRATOR"
	RoleIaaSAdministrator       = "COM_VMWARE_IAAS_IAAS_ADMINISTRATOR"
	RoleAccessControlAdmin      = "COM_VMWARE_IAAS_ACCESSCONTROLADMIN"
	RoleApprover                = "COM_VMWARE_IAAS_APPROVER"
	RoleService                 = "COM_VMWARE_IAAS_SERVICE"
	RoleFullAccessAdministrator = "COM_VMWARE_IAAS_FULL_ACCESS_ADMINISTRATOR"
	RoleTemplateUser            = "COM_VMWARE_IAAS_TEMPLATE_USER"
	RoleDEMAccess               = "COM_VMWARE_IAAS_DEM_ACCESS"
	RoleReleaseManager          = "RELEASE_MANAGER"
	RoleReleaseEngineer         = "RELEASE_ENGINEER"
)

//RoleNameMapping = list of possible ROLE_IDs... both friendly names and the actual perms are allowed
var RoleNameMapping = map[string]string{
	"SecureExportConsumer":                         "CONTENT_SERVICE_SECURE_EXPORT_CONSUMER",
	"CONTENT_SERVICE_SECURE_EXPORT_CONSUMER":       "CONTENT_SERVICE_SECURE_EXPORT_CONSUMER",
	"SoftwareArchitect":                            "SOFTWARE_SERVICE_SOFTWARE_ARCHITECT",
	"SOFTWARE_SERVICE_SOFTWARE_ARCHITECT":          "SOFTWARE_SERVICE_SOFTWARE_ARCHITECT",
	"XaaSArchitect":                                "DESIGNER_SERVICE_ARCHITECT",
	"DESIGNER_SERVICE_ARCHITECT":                   "DESIGNER_SERVICE_ARCHITECT",
	"CatalogAdministrator":                         "CATALOG_SERVICE_CATALOG_ADMIN",
	"CATALOG_SERVICE_CATALOG_ADMIN":                "CATALOG_SERVICE_CATALOG_ADMIN",
	"ContainerAdmin":                               "CONTAINER_ADMIN",
	"CONTAINER_ADMIN":                              "CONTAINER_ADMIN",
	"ContainerArchitect":                           "CONTAINER_ARCHITECT",
	"CONTAINER_ARCHITECT":                          "CONTAINER_ARCHITECT",
	"ApplicationArchitect":                         "COMPOSITION_SERVICE_APPLICATION_ARCHITECT",
	"COMPOSITION_SERVICE_APPLICATION_ARCHITECT":    "COMPOSITION_SERVICE_APPLICATION_ARCHITECT",
	"InfrastructureArchitect":                      "COMPOSITION_SERVICE_INFRASTRUCTURE_ARCHITECT",
	"COMPOSITION_SERVICE_INFRASTRUCTURE_ARCHITECT": "COMPOSITION_SERVICE_INFRASTRUCTURE_ARCHITECT",
	"HealthConsumer":                               "HEALTHBROKER_ADMIN",
	"HEALTHBROKER_ADMIN":                           "HEALTHBROKER_ADMIN",
	"RepositoryAdministrator":                      "COM_VMWARE_IAAS_REPOSITORY_ADMINISTRATOR",
	"COM_VMWARE_IAAS_REPOSITORY_ADMINISTRATOR":     "COM_VMWARE_IAAS_REPOSITORY_ADMINISTRATOR",
	"DesignCenterUser":                             "COM_VMWARE_IAAS_DESIGN_CENTER_USER",
	"COM_VMWARE_IAAS_DESIGN_CENTER_USER":           "COM_VMWARE_IAAS_DESIGN_CENTER_USER",
	"SystemAdministrator":                          "CSP_SYSTEM_ADMIN",
	"CSP_SYSTEM_ADMIN":                             "CSP_SYSTEM_ADMIN",
	"TenantAdministrator":                          "CSP_TENANT_ADMIN",
	"CSP_TENANT_ADMIN":                             "CSP_TENANT_ADMIN",
	"ApprovalAdministrator":                        "CSP_APPROVAL_ADMIN",
	"CSP_APPROVAL_ADMIN":                           "CSP_APPROVAL_ADMIN",
	"RepositoryUser":                               "COM_VMWARE_IAAS_REPOSITORY_USER",
	"COM_VMWARE_IAAS_REPOSITORY_USER":              "COM_VMWARE_IAAS_REPOSITORY_USER",
	"DEMUser":                                      "COM_VMWARE_IAAS_DEM_USER",
	"COM_VMWARE_IAAS_DEM_USER":                     "COM_VMWARE_IAAS_DEM_USER",
	"UserWithUIAccess":                             "COM_VMWARE_IAAS_USER_WITH_UI_ACCESS",
	"COM_VMWARE_IAAS_USER_WITH_UI_ACCESS":          "COM_VMWARE_IAAS_USER_WITH_UI_ACCESS",
	"WebsiteBootstrapper":                          "COM_VMWARE_IAAS_WEBSITE_BOOTSTRAPPER",
	"COM_VMWARE_IAAS_WEBSITE_BOOTSTRAPPER":         "COM_VMWARE_IAAS_WEBSITE_BOOTSTRAPPER",
	"EnterpriseAdministrator":                      "COM_VMWARE_IAAS_ENTERPRISE_ADMINISTRATOR",
	"COM_VMWARE_IAAS_ENTERPRISE_ADMINISTRATOR":     "COM_VMWARE_IAAS_ENTERPRISE_ADMINISTRATOR",
	"IaaSAdministrator":                            "COM_VMWARE_IAAS_IAAS_ADMINISTRATOR",
	"COM_VMWARE_IAAS_IAAS_ADMINISTRATOR":           "COM_VMWARE_IAAS_IAAS_ADMINISTRATOR",
	"AccessControlAdmin":                           "COM_VMWARE_IAAS_ACCESSCONTROLADMIN",
	"COM_VMWARE_IAAS_ACCESSCONTROLADMIN":           "COM_VMWARE_IAAS_ACCESSCONTROLADMIN",
	"Approver":                                     "COM_VMWARE_IAAS_APPROVER",
	"COM_VMWARE_IAAS_APPROVER":                     "COM_VMWARE_IAAS_APPROVER",
	"Service":                                      "COM_VMWARE_IAAS_SERVICE",
	"COM_VMWARE_IAAS_SERVICE":                      "COM_VMWARE_IAAS_SERVICE",
	"FullAccessAdministrator":                      "COM_VMWARE_IAAS_FULL_ACCESS_ADMINISTRATOR",
	"COM_VMWARE_IAAS_FULL_ACCESS_ADMINISTRATOR":    "COM_VMWARE_IAAS_FULL_ACCESS_ADMINISTRATOR",
	"TemplateUser":                                 "COM_VMWARE_IAAS_TEMPLATE_USER",
	"COM_VMWARE_IAAS_TEMPLATE_USER":                "COM_VMWARE_IAAS_TEMPLATE_USER",
	"DEMAccess":                                    "COM_VMWARE_IAAS_DEM_ACCESS",
	"COM_VMWARE_IAAS_DEM_ACCESS":                   "COM_VMWARE_IAAS_DEM_ACCESS",
	"ReleaseManager":                               "RELEASE_MANAGER",
	"RELEASE_MANAGER":                              "RELEASE_MANAGER",
	"ReleaseEngineer":                              "RELEASE_ENGINEER",
	"RELEASE_ENGINEER":                             "RELEASE_ENGINEER",
}

//ResourceVra7TenantRole - use to set resource fields
func ResourceVra7TenantRole() *schema.Resource {
	return &schema.Resource{
		Create: createResourceVra7TenantRole,
		Read:   readResourceVra7TenantRole,
		Update: updateResourceVra7TenantRole,
		Delete: deleteResourceVra7TenantRole,
		Schema: setResourceVra7TenantRoleSchema(),
	}
}

//setResourceVra7TenantRoleSchema - Used to set resource fileds/schema
func setResourceVra7TenantRoleSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"role_id": {
			Type:     schema.TypeString,
			Required: true,
			ValidateFunc: func(v interface{}, k string) (ws []string, errors []error) {
				_, present := RoleNameMapping[v.(string)]
				if !present {
					errors = append(errors, fmt.Errorf("%s not a valid role_id", v.(string)))
				}
				return
			},
		},
		"role_id2": {
			Type:     schema.TypeString,
			Computed: true,
		},
		"tenant_name": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"principal_ids": &schema.Schema{
			Type:     schema.TypeList,
			Required: true,
			Elem:     &schema.Schema{Type: schema.TypeString},
			// Might see about validate func for the principal_ids regexp of <x>@<y>
			//ValidateFunc: validation.StringInSlice([]string{"LDAP", "AD", "NATIVE_AD", "LOCAL_DIRECTORY"}, true),
		},
	}
}

//Function use - to set a create resource call
//Terraform call - terraform apply
func createResourceVra7TenantRole(d *schema.ResourceData, meta interface{}) error {
	//Log file handler to generate logs for debugging purpose
	log.Println("createResourceVra7TenantRole->Starting")
	//Get client handle
	client := meta.(*APIClient)
	roleInfo := new(RoleInfo)
	//Still need to sort out how we're going to handle role id/name
	roleInfo.RoleID = d.Get("role_id").(string)
	roleInfo.RoleIDRaw = RoleNameMapping[roleInfo.RoleID]
	log.Printf("createResourceVra7TenantRole->Got %s for unambiguated role name from %s", roleInfo.RoleIDRaw, roleInfo.RoleID)

	if d.Get("tenant_name").(string) != "" {
		roleInfo.TenantName = d.Get("tenant_name").(string)
	} else {
		roleInfo.TenantName = client.Tenant
	}

	principalIDs := []string{}
	for _, principal := range d.Get("principal_ids").([]interface{}) {
		principalIDs = append(principalIDs, principal.(string))
		err := client.AddVra7TenantUserToRole(principal.(string), roleInfo.RoleIDRaw, roleInfo.TenantName)
		if err != nil {
			log.Printf("createResourceVra7TenantRole->Error attempting to add %s to %s", roleInfo.RoleID, principal)
			log.Printf("createResourceVra7TenantRole->Error Message is: %v", err)
			return err
		}
	}
	roleInfo.PrincipalIDs = principalIDs

	log.Printf("createResourceVra7TenantRole->")
	uidV4, uiderr := uuid.NewV4()
	if uiderr != nil {
		fmt.Println("error:", uiderr)
		return uiderr
	}
	newid := "role_" + uidV4.String()
	log.Printf("createResourceVra7TenantRole->newID = %s",newid)
	d.SetId(newid)
	//Set request status

	return nil
}

//Function use - to update centOS 6.3 machine present in state file
//Terraform call - terraform refresh
func updateResourceVra7TenantRole(d *schema.ResourceData, meta interface{}) error {
	log.Println(d)
	return nil
}

//Function use - To read configuration of centOS 6.3 machine present in state file
//Terraform call - terraform refresh
func readResourceVra7TenantRole(d *schema.ResourceData, meta interface{}) error {
	//Get requester machine ID from schema.dataresource
	//requestMachineID := d.Id()
	//Get client handle
	//client := meta.(*APIClient)
	//Get requested status
	return nil
}

//Function use - To delete resources which are created by terraform and present in state file
//Terraform call - terraform destroy
func deleteResourceVra7TenantRole(d *schema.ResourceData, meta interface{}) error {
	//Get requester machine ID from schema.dataresource
	//requestMachineID := d.Id()
	//Get client handle
	//client := meta.(*APIClient)
	//If resource got deleted then unset the resource ID from state file
	d.SetId("")
	return nil
}

func (c *APIClient) AddVra7TenantUserToRole(principal string, role string, tenant string) error {

	log.Println("AddVra7TenantUserToRole->Starting")
	//Form a path to set a REST call to add a role to a user
	path := fmt.Sprintf("/identity/api/authorization/tenants/%s/principals/%s/roles/%s", tenant, principal, role)
	log.Printf("AddVra7TenantUserToRole-> Path = [%s]", path)

	apiError := new(APIError)
	roleInfo := new(RoleInfoJson)
	roleAdditionRes, err := c.HTTPClient.New().Put(path).Receive(roleInfo, apiError)

	// 204 Status code == Successful operation
	if roleAdditionRes.StatusCode == 204 {
		return nil
	}
	if err != nil {
		return err
	}
	if !apiError.isEmpty() {
		return apiError
	}
	//we should never get here....
	return fmt.Errorf("AddVra7TenantUserToRole->Error: Something weird happened")
}
