package vra7tenant

import (
	"errors"
	"fmt"
	"log"
)

//ErrIDStoreNotFound - Is this used?
var ErrRoleNotFound = errors.New("vra7tenant:Tenant Not Found")

type RoleInfo struct {
	RoleID       string
	RoleIDRaw    string
	TenantName   string
	PrincipalIDs interface{}
}

type RoleInfoJson struct {
	RoleID       string `json:"roleId"`
	TenantName   string `json:"tenantId"`
	PrincipalIDs string `json:"principal_ids"`
}

//GetIDStore -hhhhhh
func (c *APIClient) GetUserRole(userID string) (*UserInfo, error) {
	//Form a path to read Tenant template via REST call

	path := fmt.Sprintf("/identity/api/tenants/%s/principals/%s", c.Tenant, userID)
	log.Printf("GetIDStore->path %v\n", path)

	userInfo := new(UserInfo)
	apiError := new(APIError)
	//Set REST call to get Tenant template
	getUserRes, err := c.HTTPClient.New().Get(path).Receive(userInfo, apiError)

	if getUserRes.StatusCode != 200 {
		//Means we tried to get a tenant that doesn't exist
		log.Printf("GetTenant-> returning ErrTenantNotFound for UserID [%s]", userID)
		return nil, ErrUserNotFound
	}
	if err != nil {
		log.Printf("GetUser-> err !=nil:%v", err)
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("GetUser-> apiErr !isEmpty: %s\n", apiError.Error())
		return nil, apiError
	}

	//Return Tenant template
	log.Printf("GetUser->returning found Tenant  %v\n", userInfo)
	return userInfo, nil
}

//GetAllIDStores - set call to read Tenant item provided in terraform config file
//This compliles but hasnb't been actually user/tested yet (may be useful for import?
func (c *APIClient) GetAllUsersRoles() ([]UserInfo, error) {
	//Form a path to read Tenant template via REST call

	path := fmt.Sprintf("/identity/api/tenants")
	log.Printf("GetAllUser->path %v\n", path)

	var userInfo []UserInfo
	apiError := new(APIError)
	//Set REST call to get Tenant template
	_, err := c.HTTPClient.New().Get(path).Receive(userInfo, apiError)

	/*	if getTenantRes.StatusCode == 404 {
		//Means we tried to get a tenant that doesn't exist
		log.Printf("GetTenant-> returning ErrTenantNotFound for tenantID [%s]", tenantID)
		return nil, ErrTenantNotFound
	}*/
	if err != nil {
		log.Printf("GetAllUser-> err !=nil:%v", err)
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("GetAllUsers-> apiErr !isEmpty: %s\n", apiError.Error())
		return nil, apiError
	}

	//Return Tenant template
	log.Printf("GetAllUsers->returning found Tenant  %v\n", userInfo)
	return userInfo, nil
}
