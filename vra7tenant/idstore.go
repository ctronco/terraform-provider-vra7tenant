package vra7tenant

import (
	"errors"
	"fmt"
	"log"
)

//ErrIDStoreNotFound - Is this used?
var ErrIDStoreNotFound = errors.New("vra7tenant:Tenant Not Found")

//IDStoreInfo  - This struct holds blueprint response of Tenant
type IDStoreInfo struct {
	Domain                         string      `json:"domain"`
	Name                           string      `json:"name"`
	Description                    string      `json:"description"`
	Alias                          string      `json:"alias"`
	DirectoryType                  string      `json:"type"`
	UserNameDN                     string      `json:"userNameDn"`
	Password                       string      `json:"password"`
	URL                            string      `json:"url"`
	Subdomains                     interface{} `json:"subdomains"`
	GroupBaseSearchDNs             interface{} `json:"groupBaseSearchDns"`
	UserBaseSearchDNs              interface{} `json:"userBaseSearchDns"`
	DomainAdminUsername            string      `json:"domainAdminUserName"`
	DomainAdminPassword            string      `json:"domainAdminPassword"`
	Certificate                    string      `json:"Certificate"`
	TrustAll                       bool        `json:"trustAll"`
	UseGlobalCatalog               bool        `json:"useGlobalCatalog"`
	GroupObjectQuery               string      `json:"groupObjectQuery"`
	BindUserObjectQuery            string      `json:"bindUserObjectQuery"`
	UserObjectQuery                string      `json:"userObjectQuery"`
	CustomDirectorySearchAttribute string      `json:"customDirectorySearchAttribute"`
	MembershipAttribute            string      `json:"membershipAttribute"`
	ObjectUUIDAttribute            string      `json:"objectUuidAttribute"`
	DistinguishedNameAttribute     string      `json:"distinguishedNameAttribute"`
	New                            bool        `json:"new"`
	//these two values can be specified but are now ignore by vRA
	//	groupBaseSearchDn              string  `json:"id"`
	//	userBaseSearchDn               string  `json:"id"`

}

//GetIDStore -hhhhhh
func (c *APIClient) GetIDStore(idStoreName string) (*IDStoreInfo, error) {
	//Form a path to read Tenant template via REST call

	path := fmt.Sprintf("/identity/api/tenants/%s/directories/%s", c.Tenant, idStoreName)
	log.Printf("GetIDStore->path %v\n", path)

	idStoreInfo := new(IDStoreInfo)
	apiError := new(APIError)
	//Set REST call to get Tenant template
	getIDStoreRes, err := c.HTTPClient.New().Get(path).Receive(idStoreInfo, apiError)

	if getIDStoreRes.StatusCode != 200 {
		//Means we tried to get a tenant that doesn't exist
		log.Printf("GetTenant-> returning ErrTenantNotFound for tenantID [%s]", idStoreName)
		return nil, ErrIDStoreNotFound
	}
	if err != nil {
		log.Printf("GetTenant-> err !=nil:%v", err)
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("GetTenant-> apiErr !isEmpty: %s\n", apiError.Error())
		return nil, apiError
	}

	//Return Tenant template
	log.Printf("GetTenant->returning found Tenant  %v\n", idStoreInfo)
	return idStoreInfo, nil
}

//GetAllIDStores - set call to read Tenant item provided in terraform config file
//This compliles but hasnb't been actually user/tested yet (may be useful for import?
func (c *APIClient) GetAllIDStores() ([]TenantInfo, error) {
	//Form a path to read Tenant template via REST call

	path := fmt.Sprintf("/identity/api/tenants")
	log.Printf("GetAllTenant->path %v\n", path)

	var tenantInfo []TenantInfo
	apiError := new(APIError)
	//Set REST call to get Tenant template
	_, err := c.HTTPClient.New().Get(path).Receive(tenantInfo, apiError)

	/*	if getTenantRes.StatusCode == 404 {
		//Means we tried to get a tenant that doesn't exist
		log.Printf("GetTenant-> returning ErrTenantNotFound for tenantID [%s]", tenantID)
		return nil, ErrTenantNotFound
	}*/
	if err != nil {
		log.Printf("GetTenant-> err !=nil:%v", err)
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("GetTenant-> apiErr !isEmpty: %s\n", apiError.Error())
		return nil, apiError
	}

	//Return Tenant template
	log.Printf("GetTenant->returning found Tenant  %v\n", tenantInfo)
	return tenantInfo, nil
}
