package vra7tenant

import (
	"errors"
	"fmt"
	"log"
	//	"reflect"
	//	"strings"

	"encoding/json"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/helper/validation"
)

//ResourceVra7TenantIdStore - use to set resource fields
func ResourceVra7TenantIdStore() *schema.Resource {
	return &schema.Resource{
		Create: createResourceVra7TenantIDStore,
		Read:   readResourceVra7TenantIDStore,
		Update: updateResourceVra7TenantIDStore,
		Delete: deleteResourceVra7TenantIDStore,
		Schema: setResourceVra7TenantIDStoreSchema(),
	}
}

//set_resource_schema - This function is used to update the catalog template/blueprint
//and replace the values with user defined values added in .tf file.
func setResourceVra7TenantIDStoreSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"domain": {
			Type:     schema.TypeString,
			Required: true,
		},
		"name": {
			Type:     schema.TypeString,
			Required: true,
		},
		"description": {
			Type:        schema.TypeString,
			Optional:    true,
			Description: "A description for this ID Store",
		},
		"alias": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"directory_type": {
			Type:         schema.TypeString,
			Required:     false,
			Optional:     true,
			Default:      "AD",
			ValidateFunc: validation.StringInSlice([]string{"LDAP", "AD", "NATIVE_AD", "LOCAL_DIRECTORY"}, true),
		},
		"bind_user_dn": {
			Type:     schema.TypeString,
			Required: true,
			Description: "The distinguishedName of the user that authenticates to this ID store " +
				"on behalf of this tenant",
		},
		"bind_user_password": {
			Type:     schema.TypeString,
			Required: true,
		},
		"url": {
			Type:     schema.TypeString,
			Required: true,
		},
		"subdomains": &schema.Schema{
			Type:     schema.TypeList,
			Optional: true,
			Elem:     &schema.Schema{Type: schema.TypeString},
		},
		"group_base_search_dns": &schema.Schema{
			Type:     schema.TypeList,
			Optional: true,
			Elem:     &schema.Schema{Type: schema.TypeString},
		},
		"user_base_search_dns": &schema.Schema{
			Type:     schema.TypeList,
			Optional: true,
			Elem:     &schema.Schema{Type: schema.TypeString},
		},
		"use_global_catalog": {
			Type:     schema.TypeBool,
			Optional: true,
			Default:  "false",
		},
		"domainadmin_username": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"domainadmin_password": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"new": {
			Type:     schema.TypeBool,
			Optional: true,
		},
		"certificate": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"trust_all": {
			Type:     schema.TypeBool,
			Optional: true,
		},
		"group_object_query": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"binduser_object_query": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"user_object_query": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"custom_directory_search_attribute": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"membership_attribute": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"object_uuid_attribute": {
			Type:     schema.TypeString,
			Optional: true,
		},
		"distinguishedname_attribute": {
			Type:     schema.TypeString,
			Optional: true,
		},
	}
}

//Terraform call - terraform apply
//createResourceVra7TenantIDStore
func createResourceVra7TenantIDStore(d *schema.ResourceData, meta interface{}) error {

	client := meta.(*APIClient)
	log.Println("createResourceVra7TenantIDStore-> Starting")

	// Do we need to check if the idstore exists or not?
	idStoreInfo := new(IDStoreInfo)
	// Domain is the key/id for directories
	idStoreInfo.Domain = d.Get("domain").(string)
	idStoreInfo.Name = d.Get("name").(string)
	idStoreInfo.Description = d.Get("description").(string)
	idStoreInfo.DirectoryType = d.Get("directory_type").(string)
	idStoreInfo.UserNameDN = d.Get("bind_user_dn").(string)
	idStoreInfo.Password = d.Get("bind_user_password").(string)
	idStoreInfo.URL = d.Get("url").(string)

	groupDNs := []string{}
	for _, dn := range d.Get("group_base_search_dns").([]interface{}) {
		groupDNs = append(groupDNs, dn.(string))
	}
	idStoreInfo.GroupBaseSearchDNs = groupDNs

	userDNs := []string{}
	for _, dn := range d.Get("group_base_search_dns").([]interface{}) {
		userDNs = append(userDNs, dn.(string))
	}
	idStoreInfo.UserBaseSearchDNs = userDNs

	subdomains := []string{}
	for _, domain := range d.Get("subdomains").([]interface{}) {
		subdomains = append(subdomains, domain.(string))
	}
	idStoreInfo.Subdomains = subdomains
	//idStoreInfo.Password = d.Get("bind_user_password").(string)

	idStoreInfo.DomainAdminUsername = d.Get("domainadmin_username").(string)
	idStoreInfo.DomainAdminPassword = d.Get("domainadmin_password").(string)
	idStoreInfo.Certificate = d.Get("certificate").(string)
	idStoreInfo.TrustAll = d.Get("trust_all").(bool)
	idStoreInfo.UseGlobalCatalog = d.Get("use_global_catalog").(bool)
	idStoreInfo.GroupObjectQuery = d.Get("group_object_query").(string)
	idStoreInfo.BindUserObjectQuery = d.Get("binduser_object_query").(string)
	idStoreInfo.UserObjectQuery = d.Get("user_object_query").(string)
	idStoreInfo.CustomDirectorySearchAttribute = d.Get("custom_directory_search_attribute").(string)
	idStoreInfo.MembershipAttribute = d.Get("membership_attribute").(string)
	idStoreInfo.ObjectUUIDAttribute = d.Get("object_uuid_attribute").(string)
	idStoreInfo.DistinguishedNameAttribute = d.Get("distinguishedname_attribute").(string)
	idStoreInfo.New = d.Get("new").(bool)

	jsonBody, jErr := json.Marshal(idStoreInfo)
	if jErr != nil {
		log.Printf("createResourceVra7TenantIDStore->jErr= %s", jErr)
	}
	log.Printf("createResourceVra7TenantIDStore-> %s", jsonBody)
	requestIDStore, err := client.CreateVra7TenantIDStore(idStoreInfo)

	if err != nil {
		log.Printf("createResourceVra7TenantIDStore-> err !=nil:%v", err)
		return err
	}
	log.Printf("createResourceVra7TenantIDStore %s", requestIDStore.Domain)

	//Set request ID
	d.SetId(d.Get("domain").(string))
	return nil
}

//updateResourceVra7TenantIDStore
//Function use - to update centOS 6.3 machine present in state file
//Terraform call - terraform refresh
func updateResourceVra7TenantIDStore(d *schema.ResourceData, meta interface{}) error {
	log.Println("updateResourceVra7TenantIDStore->Starting")
	readIDStoreID := d.Id()
	client := meta.(*APIClient)
	oldIDStoreInfo, err := client.GetIDStore(readIDStoreID)
	if err != nil {
		return fmt.Errorf("Resource view failed to load:  %v", err)
	}
	log.Printf("updateResourceVra7TenantIDStore->Old TenantInfo: %v", oldIDStoreInfo)

	log.Printf("updateResourceVra7TenantIDStore->UpdatingTenant")
	// Do we need to check if the idstore exists or not?
	idStoreInfo := new(IDStoreInfo)
	// Domain is the key/id for directories, so re-use old one
	idStoreInfo.Domain = oldIDStoreInfo.Domain
	idStoreInfo.Name = d.Get("name").(string)
	idStoreInfo.Description = d.Get("description").(string)
	idStoreInfo.DirectoryType = d.Get("directory_type").(string)
	idStoreInfo.UserNameDN = d.Get("bind_user_dn").(string)
	idStoreInfo.Password = d.Get("bind_user_password").(string)
	idStoreInfo.URL = d.Get("url").(string)

	groupDNs := []string{}
	for _, dn := range d.Get("group_base_search_dns").([]interface{}) {
		groupDNs = append(groupDNs, dn.(string))
	}
	idStoreInfo.GroupBaseSearchDNs = groupDNs

	userDNs := []string{}
	for _, dn := range d.Get("group_base_search_dns").([]interface{}) {
		userDNs = append(userDNs, dn.(string))
	}
	idStoreInfo.UserBaseSearchDNs = userDNs

	subdomains := []string{}
	for _, domain := range d.Get("subdomains").([]interface{}) {
		subdomains = append(subdomains, domain.(string))
	}
	idStoreInfo.Subdomains = subdomains
	//idStoreInfo.Password = d.Get("bind_user_password").(string)

	idStoreInfo.DomainAdminUsername = d.Get("domainadmin_username").(string)
	idStoreInfo.DomainAdminPassword = d.Get("domainadmin_password").(string)
	idStoreInfo.Certificate = d.Get("certificate").(string)
	idStoreInfo.TrustAll = d.Get("trust_all").(bool)
	idStoreInfo.UseGlobalCatalog = d.Get("use_global_catalog").(bool)
	idStoreInfo.GroupObjectQuery = d.Get("group_object_query").(string)
	idStoreInfo.BindUserObjectQuery = d.Get("binduser_object_query").(string)
	idStoreInfo.UserObjectQuery = d.Get("user_object_query").(string)
	idStoreInfo.CustomDirectorySearchAttribute = d.Get("custom_directory_search_attribute").(string)
	idStoreInfo.MembershipAttribute = d.Get("membership_attribute").(string)
	idStoreInfo.ObjectUUIDAttribute = d.Get("object_uuid_attribute").(string)
	idStoreInfo.DistinguishedNameAttribute = d.Get("distinguishedname_attribute").(string)
	idStoreInfo.New = d.Get("new").(bool)
	jsonBody, jErr := json.Marshal(idStoreInfo)
	if jErr != nil {
		log.Printf("updateResourceVra7TenantIDStore->jErr= %s", jErr)
	}
	log.Printf("updateResourceVra7TenantIDStore-> %s", jsonBody)
	requestIDStore, err := client.CreateVra7TenantIDStore(idStoreInfo)

	if err != nil {
		log.Printf("updateResourceVra7TenantIDStore-> err !=nil:%v", err)
		return err
	}
	log.Printf("updateResourceVra7TenantIDStore %s", requestIDStore.Domain)

	//Set request ID
	return nil
}

//readResourceVra7TenantIDStore
//Function use - To read configuration of centOS 6.3 machine present in state file
//Terraform call - terraform refresh
func readResourceVra7TenantIDStore(d *schema.ResourceData, meta interface{}) error {
	log.Println("readResourceVra7TenantIDStore->Starting")
	readIDStoreID := d.Id()
	client := meta.(*APIClient)
	idStoreInfo, err := client.GetIDStore(readIDStoreID)
	if err != nil {
		log.Printf("readResourceVra7TenantIDStore->Err Getting %s : %v", readIDStoreID, err)
		return fmt.Errorf("Resource view failed to load for IDStore %s : %v", readIDStoreID, err)
	}
	// Domain is the key/id for directories, so re-use old one
	d.Set("domain", idStoreInfo.Domain)
	d.Set("name", idStoreInfo.Name)
	d.Set("description", idStoreInfo.Description)
	d.Set("directory_type", idStoreInfo.DirectoryType)
	d.Set("bind_user_dn", idStoreInfo.UserNameDN)
	d.Set("bind_user_password", idStoreInfo.Password)
	d.Set("url", idStoreInfo.URL)

	//The array values may be problematic???
	d.Set("group_base_search_dns", idStoreInfo.GroupBaseSearchDNs)
	d.Set("group_base_search_dns", idStoreInfo.UserBaseSearchDNs)
	d.Set("subdomains", idStoreInfo.Subdomains)

	d.Set("domainadmin_username", idStoreInfo.DomainAdminUsername)
	d.Set("domainadmin_password", idStoreInfo.DomainAdminPassword)
	d.Set("certificate", idStoreInfo.Certificate)
	d.Set("trust_all", idStoreInfo.TrustAll)
	d.Set("use_global_catalog", idStoreInfo.UseGlobalCatalog)
	d.Set("group_object_query", idStoreInfo.GroupObjectQuery)
	d.Set("binduser_object_query", idStoreInfo.BindUserObjectQuery)
	d.Set("user_object_query", idStoreInfo.UserObjectQuery)
	d.Set("custom_directory_search_attribute", idStoreInfo.CustomDirectorySearchAttribute)
	d.Set("membership_attribute", idStoreInfo.MembershipAttribute)
	d.Set("object_uuid_attribute", idStoreInfo.ObjectUUIDAttribute)
	d.Set("distinguishedname_attribute", idStoreInfo.DistinguishedNameAttribute)
	d.Set("new", idStoreInfo.New)
	log.Println("readResourceVra7TenantIDStore->Ending")
	return nil
}

//Function use - To delete resources which are created by terraform and present in state file
//Terraform call - terraform destroy
func deleteResourceVra7TenantIDStore(d *schema.ResourceData, meta interface{}) error {

	//Get requester Tenant ID from schema.dataresource
	idStoreID := d.Id()
	//Get client handle
	client := meta.(*APIClient)
	log.Printf("deleteResourceVra7TenantIDStore->deleting IDstore %s", idStoreID)
	idStoreInfo, err := client.GetIDStore(idStoreID)
	if err != nil {
		log.Printf("deleteResourceVra7TenantIDStore-> IDStore [%s] Not Found", idStoreID)
		return fmt.Errorf("Resource not found")
	}

	log.Printf("deleteResourceVra7TenantIDStore->trying to delete IDstore %s", idStoreInfo.Domain)
	err = client.DestroyVra7TenantIDStore(idStoreInfo)
	//Error
	if err != nil {
		log.Printf("deleteResourceVra7TenantIDStore->Err Deleting %s : %v", idStoreID, err)
		return err
	}

	// No error, tenant was deleted successfully
	log.Printf("deleteResourceVra7TenantIDStore->IDStore %s deleted", idStoreID)
	d.SetId("")
	return nil
}

//CreateVra7TenantIDStore - To set create resource REST call/Also works for update
func (c *APIClient) CreateVra7TenantIDStore(idStoreInfo *IDStoreInfo) (*IDStoreInfo, error) {

	log.Println("CreateVra7TenantIDStore->Starting")
	//Form a path to set a REST call to create a machine
	path := fmt.Sprintf("/identity/api/tenants/%s/directories", c.Tenant)
	//path := fmt.Sprintf("https://{{vraHost}}/identity/api/tenants/%s/directories", idStoreInfo)

	requestMachineRes := new(IDStoreInfo)
	apiError := new(APIError)

	jsonBody, jErr := json.Marshal(idStoreInfo)
	if jErr != nil {
		log.Printf("Error marshalling template as JSON")
		return nil, jErr
	}
	log.Printf("JSON Request Info: %s", jsonBody)

	//Set a REST call to create a machine
	_, err := c.HTTPClient.New().Post(path).BodyJSON(idStoreInfo).
		Receive(requestMachineRes, apiError)

	if err != nil {
		return nil, err
	}

	if !apiError.isEmpty() {
		return nil, apiError
	}

	return requestMachineRes, nil
}

//RequestVra7Tenant - To set create resource REST call
func (c *APIClient) DestroyVra7TenantIDStore(idStoreInfo *IDStoreInfo) error {
	//Form a path to set a REST call to create a Tenant
	path := fmt.Sprintf("/identity/api/tenants/%s/directories/%s", idStoreInfo.Domain)

	apiError := new(APIError)
	apiSuccess := new(TenantInfo)
	//Set a REST call to create a Tenant
	//createRes, err := c.HTTPClient.New().Delete(path).BodyJSON(tenantInfo).
	destroyRes, err := c.HTTPClient.New().Delete(path).Receive(apiSuccess, apiError)

	//Expect a status code of 204 if the delete was successful.
	if destroyRes.StatusCode == 204 {
		//delete was successful
		return nil
	} else if destroyRes.StatusCode == 404 {
		//ID Store was not found
		return errors.New("Identity store with the provided ID was not found")
	} else if destroyRes.StatusCode == 400 {
		return errors.New("Identity store not deleted. If NATIVE_AD in default tenant may need to leave domain first")
	}

	log.Printf("destroyResponse was: %v", destroyRes)
	if err != nil {
		log.Printf("Error DELETEing: %v", err)
		return err
	}

	return errors.New("Something weird happened")
}
