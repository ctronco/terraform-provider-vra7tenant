package vra7tenant

import (
	"fmt"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/terraform"
)

//Provider - This function initializes the provider schema
//also the config function and resource mapping
func Provider() terraform.ResourceProvider {
	return &schema.Provider{
		Schema:        providerSchema(),
		ConfigureFunc: providerConfig,
		ResourcesMap:  providerResources(),
	}
}

//providerSchema - To set provider fields
func providerSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"tenant_admin_name": {
			Type:        schema.TypeString,
			Required:    true,
			Description: "Tenant administrator username.",
		},
		"tenant_admin_password": {
			Type:        schema.TypeString,
			Required:    true,
			Description: "Tenant administrator password.",
		},
		"tenant_name": {
			Type:     schema.TypeString,
			Required: true,
			Description: "Specifies the tenant URL token determined by the system administrator" +
				"when creating the tenant, for example, support.",
		},
		"host": {
			Type:     schema.TypeString,
			Required: true,
			Description: "host name.domain name of the vRealize Automation server, " +
				"for example, mycompany.mktg.mydomain.com.",
		},
		"insecure": {
			Type:        schema.TypeBool,
			Default:     true,
			Optional:    true,
			Description: "Specify whether to validate TLS certificates.",
		},
		"system_tenant": {
			Type:        schema.TypeBool,
			Optional:    true,
			Default:     false,
			Description: "Whether this is the system tenant - only one allowed to create tenants",
		},
	}
}

//Function use - To authenticate terraform provider
func providerConfig(r *schema.ResourceData) (interface{}, error) {
	//Create a client handle to perform REST calls for various operations upon the resource
	client := NewClient(r.Get("tenant_admin_name").(string),
		r.Get("tenant_admin_password").(string),
		r.Get("tenant_name").(string),
		r.Get("host").(string),
		r.Get("insecure").(bool),
		r.Get("system_tenant").(bool),
	)

	//Authenticate user
	err := client.Authenticate()

	//Raise an error on authentication fail
	if err != nil {
		return nil, fmt.Errorf("Error: Unable to get auth token: %v", err)
	}

	//Return client handle on success
	return &client, nil
}

//Function use - set machine resource details based on machine type
func providerResources() map[string]*schema.Resource {
	return map[string]*schema.Resource{
		"vra7tenant":         ResourceVra7Tenant(),
		"vra7tenant_user":    ResourceVra7TenantUser(),
		"vra7tenant_role":    ResourceVra7TenantRole(),
		"vra7tenant_idstore": ResourceVra7TenantIdStore(),
		//"vra7tenant_businessgroup": ResourceVra7TenantBusinessGroup(),
	}
}
