package vra7tenant

import (
	"errors"
	"fmt"
	"log"
)

//ErrIDStoreNotFound - Is this used?
var ErrUserNotFound = errors.New("vra7tenant:Tenant Not Found")

/*//UserInfo  - This struct holds blueprint response of Tenant
type UserInfo2 struct {
	FirstName    string                 `json:"firstName"`
	LastName     string                 `json:"lastName"`
	Description  string                 `json:"description"`
	EmailAddress string                 `json:"emailAddress"`
	Locked       bool                   `json:"locked"`
	Disabled     bool                   `json:"disabled"`
	Password     string                 `json:"password"`
	PrincipalID  map[string]interface{} `json:"principalId"`
	TenantName   string                 `json:"tenantName"`
	Name         string                 `json:"name"`
}
*/
type UserInfo struct {
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	Description  string `json:"description"`
	EmailAddress string `json:"emailAddress"`
	Locked       bool   `json:"locked"`
	Disabled     bool   `json:"disabled"`
	Password     string `json:"password"`
	PrincipalID  struct {
		Domain string `json:"domain"`
		Name   string `json:"name"`
	} `json:"principalId"`
	TenantName string `json:"tenantName"`
	Name       string `json:"name"`
}

//GetIDStore -hhhhhh
func (c *APIClient) GetUser(userID string) (*UserInfo, error) {
	//Form a path to read Tenant template via REST call

	path := fmt.Sprintf("/identity/api/tenants/%s/principals/%s", c.Tenant, userID)
	log.Printf("GetIDStore->path %v\n", path)

	userInfo := new(UserInfo)
	apiError := new(APIError)
	//Set REST call to get Tenant template
	getUserRes, err := c.HTTPClient.New().Get(path).Receive(userInfo, apiError)

	if getUserRes.StatusCode != 200 {
		//Means we tried to get a tenant that doesn't exist
		log.Printf("GetTenant-> returning ErrTenantNotFound for UserID [%s]", userID)
		return nil, ErrUserNotFound
	}
	if err != nil {
		log.Printf("GetUser-> err !=nil:%v", err)
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("GetUser-> apiErr !isEmpty: %s\n", apiError.Error())
		return nil, apiError
	}

	//Return Tenant template
	log.Printf("GetUser->returning found Tenant  %v\n", userInfo)
	return userInfo, nil
}

//GetAllIDStores - set call to read Tenant item provided in terraform config file
//This compliles but hasnb't been actually user/tested yet (may be useful for import?
func (c *APIClient) GetAllUsers() ([]UserInfo, error) {
	//Form a path to read Tenant template via REST call

	path := fmt.Sprintf("/identity/api/tenants")
	log.Printf("GetAllUser->path %v\n", path)

	var userInfo []UserInfo
	apiError := new(APIError)
	//Set REST call to get Tenant template
	_, err := c.HTTPClient.New().Get(path).Receive(userInfo, apiError)

	/*	if getTenantRes.StatusCode == 404 {
		//Means we tried to get a tenant that doesn't exist
		log.Printf("GetTenant-> returning ErrTenantNotFound for tenantID [%s]", tenantID)
		return nil, ErrTenantNotFound
	}*/
	if err != nil {
		log.Printf("GetAllUser-> err !=nil:%v", err)
		return nil, err
	}

	if !apiError.isEmpty() {
		log.Printf("GetAllUsers-> apiErr !isEmpty: %s\n", apiError.Error())
		return nil, apiError
	}

	//Return Tenant template
	log.Printf("GetAllUsers->returning found Tenant  %v\n", userInfo)
	return userInfo, nil
}
