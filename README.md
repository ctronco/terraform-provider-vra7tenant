# VMware Terraform provider for vRealize Automation 7 Tenant Administration

[![Build Status](https://travis-ci.org/vmware/terraform-provider-vra7tenant.svg?branch=master)](https://travis-ci.org/vmware/terraform-provider-vra7tenant)

A self-contained deployable integration between Terraform and vRealize Automation (vRA) which allows Terraform users to manage the configuration of tenants in an instance of vRA.

## Getting Started

These instructions will get you a copy of the project up and run on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

To make plugin up and running you need following things.

* [Terraform 9.8 or above](https://www.terraform.io/downloads.html)
* [Go Language 1.9.2 or above](https://golang.org/dl/)
* [dep - new dependency management tool for Go](https://github.com/golang/dep)

## Project Setup

Setup a GOLang project structure

```
|-/home/<USER>/TerraformPluginProject
    |-bin
    |-pkg
    |-pkg

```

## Environment Setup

Set the following environment variables

### Linux Users

*GOROOT is a golang library path*

```shell
export GOROOT=/usr/local/go
```

*GOPATH is a path pointing toward the source code directory*

```shell
export GOPATH=/home/<user>/TerraformPluginProject
```

### Windows Users

*GOROOT is a golang library path*

```cmd
set GOROOT=C:\Go
```

*GOPATH is a path pointing toward the source code directory*

```cmd
set GOPATH=C:\TerraformPluginProject
```

## Set terraform provider

**Linux Users**

Create *~/.terraformrc* and put following content in it.

```hcl
    providers {
         vra7tenant = "/home/<USER>/TerraformPluginProject/bin/terraform-provider-vra7tenant"
    }
```

**Windows Users**

Create *%APPDATA%/terraform.rc* and put following content in it.

```hcl
    providers {
         vra7tenant = "C:\\TerraformPluginProject\\bin\\terraform-provider-vra7tenant.exe"
    }
```


## Installation
Clone repo code into go project using *go get*
```
    go get gitlab.com/ctronco/terraform-provider-vra7

```

## Create Binary

**Linux and MacOS Users**

Navigate to */home/<USER>/TerraformPluginProject/src/github.com/vmware/terraform-provider-vra7* and run go build command to generate plugin binary

```
    dep ensure
    go build -o /home/<USER>/TerraformPluginProject/bin/terraform-provider-vra7tenant

```

## Windows Users

Navigate to *C:\TerraformPluginProject\src\github.com\vmware\terraform-provider-vra7* and run go build command to generate plugin binary

```shell
    dep ensure
    go build -o C:\TerraformPluginProject\bin\terraform-provider-vra7tenant.exe

```

## Create Terraform Configuration file

In VMware vRA terraform configuration file contains two objects

### Provider

This part contains service provider details.

#### Configure Provider

Provider block contains four mandatory fields

* **username** - *vRA portal username*
* **password** - *vRA portal password*
* **tenant** - *vRA portal tenant*
* **host** - *End point of REST API*

Example

```terraform
    provider "vra7tenant" {
      username = "vRAUser1@vsphere.local"
      password = "password123!"
      tenant = "corp.local.tenant"
      host = "http://myvra.example.com/"
    }

```

### Resources

This part contains any resource that can be deployed on that service provider.
For example, in our this could be tenants, users, groups etc.

#### Configure Resources

##### Tenants

To configure a tenant resource

Syntax

```tf
resource "vra7tenant" "<resource_name1>" {
    tenant_id = "TenantXYZ"
    url_name = "XYZ"
    name = "TenantXYZ"
    description = "vRA Tenant for group XYZ at example.com"
    contact_email = "vraadmins@example.com"
  }
```

name,desc,url,email

defaultenant,desc,contact,urlname,p/w, id,name 

Tenant {
defaultTenant (boolean, optional),
description (string, optional),
contactEmail (string, optional),
urlName (string),
password (string, optional),
id (string),
name (string)
}
  


Resource block contains two mandatory and three optional fields as follows

* **tenant_id** - *tenant_id is a unique string/name for the tenant. Typically this and 'name' are the same*

* **url_name** - *Required, this represents the string appended to https://<vrafqdn>/vcac/org/ when users reference the web interface*

* **name** - *This is optional field. If blueprint properties have default values or no mandatory property value is required then you can skip this field from terraform configuration file. This field contains user inputs to catalog services. Value of this field is in key value pair. Key is service.field_name and value is any valid user input to the respective field.*

* **description** - *This is an optional field. Place to put a more verbose description of the tenant.*

* **contact_email** - *This field is optional and provides an email address users can reference.*

* **default_tenant** - *Whether this is the default tenant or not (Typically vsphere.local) Required when performing operations against the default tenant, otherwise optional*

Example 1

```hcl

resource "vra7tenant" "<resource_name1>" {
    tenant_id = "TenantXYZ"
    url_name = "XYZ"
    name = "TenantXYZ"
    description = "vRA Tenant for group XYZ at example.com"
    contact_email = "vraadmins@example.com"
  }
```

Example 2

```hcl
resource "vra7tenant" "<resource_name1>" {
    tenant_id = "vsphere.local"
    url_name = "vsphere.local"
    name = "vsphere.local"
    default_tenant = true
  }
```
##### Users



##### Groups

##### Directory Services

Save this configuration in main.tf in a path where the binary is placed.

## Execution

These are the terraform commands that can be used on vRA plugin as follows.
* **terraform init** - *The init command is used to initialize a working directory containing Terraform configuration files.*

* **terraform plan** - *Plan command shows plan for resources like how many resources will be provisioned and how many will be destroyed.*

* **terraform apply** - *apply is responsible to execute actual calls for provision resources.*

* **terraform refresh** - *By using refresh command you can check status of request.*

* **terraform show** - *show will set a console output for resource configuration and request status.*

* **terraform destroy** - *destroy command will destroy all the  resources present in terraform configuration file.*

* **terraform update** - *destroy command will destroy all the  resources present in terraform configuration file.*

Navigate to the location where main.tf and binary are placed and use above commands as needed.

## Contributing

TBD

## License

terraform-provider-vra7tenant is available under the [MIT license](LICENSE).
