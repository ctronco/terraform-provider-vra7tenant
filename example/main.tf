provider  "vra_tenant" {
    system_username = "${var.systemUserName}"
    systemuser_password  = "${var.systemUserPassword}"
    tenant = "${var.tenant}"
    host = "${var.host}"
    tenant_username = "${var.tenantAdminLogin}"
    tenant_password  = "${var.tenantAdminPassword}"
    tenant_id = "${var.tenantId}"
    host = "${var.host}"

resource "vra_tenant_resource" "" {
  count            = 1
  catalog_name = "CentOS 7.0 x64"
  resource_configuration = {
    Linux.cpu = "2"
  }
}

resource "vra7_tenant" "myFirstTenant" {
  count            = 1
  
  #resource_configuration = {
  tenant_configuration = {
    tenant.id = "FRED"
    tenant.url = "fred"
    tenant.name = "Tenant Name"
    tenant.description = "This is my test description"
    tenant.contactemail = "cars@pobox.com"
  }
  
}
