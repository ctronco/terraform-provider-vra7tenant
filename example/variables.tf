variable systemUserName {
    type = "string"
    description = "System User Name that has permissions to create tenants"
    default = "administrator@vsphere.local"
}
variable systemUserPassword {
    type = "string"
    description = "Password for the system user"
}
variable tenant {
    type = "string"
    description = "the base/default tenant"
    default = "vsphere.local"
}
variable tenantId {
    ""
}
variable tenantAdminLogin {}
variable tenantAdminPassword {}
variable host {}
