package main

import (
	"github.com/hashicorp/terraform/plugin"
	"gitlab.com/ctronco/terraform-provider-vra7tenant/vra7tenant"
)

func main() {
	//	opts := plugin.ServeOpts{
	//		ProviderFunc: func() terraform.ResourceProvider {
	//			return vra7tenant.Provider()
	//		},
	//	}

	//	plugin.Serve(&opts)
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: vra7tenant.Provider})
}
